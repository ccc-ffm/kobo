#!/bin/sh

arm-linux-gnueabi-gcc -Wall -Wextra -pedantic -D_XOPEN_SOURCE -std=c99 -I/usr -o kobomenu kobomenu.c koboimg.c
arm-linux-gnueabi-gcc -Wall -Wextra -pedantic -D_XOPEN_SOURCE -std=c99 -I/usr -o koboprint koboprint.c koboimg.c
arm-linux-gnueabi-gcc -Wall -Wextra -pedantic -D_XOPEN_SOURCE -std=c99 -I/usr -o koboread koboread.c
