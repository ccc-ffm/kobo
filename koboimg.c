#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "koboimg.h"
#include "font24.h"

unsigned char img[800][800];
unsigned char img565[600][800][2];

int width, height;

void init(int landscape)
{
   width = landscape ? 800 : 600;
   height = landscape ? 600 : 800;
   
   memset(img, 255, 800 * 800);
}

int getWidth(void)
{
   return width;
}

int getHeight(void)
{
   return height;
}

void imgLine(int x, int y, int xx, int yy)
{
   int x0 = (x), y0 = (y), x1 = (xx), y1 = (yy);
   int dx =  abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
   int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
   int err = dx + dy, e2;

   for(;;)
   {
      if(x0 >= 0 && x0 < width && y0 >= 0 && y0 <height) img[y0][x0] = 0;
      if (x0 == x1 && y0 == y1) break;
      e2 = 2 * err;
      if (e2 >= dy) {err += dy; x0 += sx;}
      if (e2 <= dx) {err += dx; y0 += sy;}
   }
}

void imgBox(int x, int y, int xx, int yy){
   imgLine(x,y,xx,y);
   imgLine(x,y,x,yy);
   imgLine(xx,y,xx,yy);
   imgLine(x,yy,xx,yy);
}

int linew(const char* str)
{
   int thisline = 0;
   for(; *str && *str != '\n'; str++)
   {
      if(*str >= 32 && *str <= 127) thisline++;
   }

   return FONT_STRIDE * thisline + FONT_WIDTH - FONT_STRIDE;
}

int strh(char* str)
{
   int lines = 1;
   for(; *str; str++)
   {
      if(*str == '\n')
      {
         lines++;
      }
   }

   return lines * FONT_HEIGHT;
}

BoundingBox drawString(char* str, int x, int y, int centered, int border)
{
   const char *p;
   int x0 = x;
   int y0 = y;
   BoundingBox bb = {INT_MAX, INT_MAX, INT_MIN, INT_MIN};
   
   if (centered)
   {
      x = x0 - linew(str) / 2;
      y = y0 - strh(str) / 2;
   }

   for (p = str; *p; ++p)
   {
      if(*p == '\n')
      {
         x = x0 - (centered ? linew(p + 1) / 2 : 0);
         y += FONT_HEIGHT;
         continue;
      }
      if(*p < 32 || *p > 127)
      {
         continue;
      }
      
      int xmin, xmax, ymin, ymax;
      xmin = x;
      ymin = y;
      xmax = x + FONT_WIDTH - 1;
      ymax = y + FONT_HEIGHT - 1;

      if(xmin >= border && ymin >= border && xmax < (width - border) && ymax < (height - border))
      {
         for(int yl = 0; yl < FONT_HEIGHT; yl++)
         {
            for(int xl = 0; xl < FONT_WIDTH; xl++)
            {
               int xll, yll;
               xll = x + xl;
               yll = y + yl;
               
               int v1 = 255 - img[yll][xll];
               int v2 = 255 - font[yl][(*p - 32) * 16 + xl];
               int target = 255 - (v1 + v2);
               if(target < 0) target = 0;
               img[yll][xll] = target;
               if(xll < bb.x0) bb.x0 = xll;
               if(yll < bb.y0) bb.y0 = yll;
               if(xll > bb.x1) bb.x1 = xll;
               if(yll > bb.y1) bb.y1 = yll;
            }
         }
      }
      x += FONT_STRIDE;
   }
   return bb;
}

unsigned char* create565(int landscape)
{
   for(int y = 0; y < 600; y++)
   {
      for(int x = 0; x < 800; x++)
      {
         int xl = landscape ? x : 599 - y;
         int yl = landscape ? y : x;
         img565[y][x][0] = 0;
         img565[y][x][1] = 0;
         img565[y][x][1] |= (img[yl][xl] / 8) << 3;
         img565[y][x][1] |= (img[yl][xl] / 4) >> 3;
         img565[y][x][0] |= (img[yl][xl] / 4) << 5;
         img565[y][x][0] |= (img[yl][xl] / 8);
      }
   }
   return &img565[0][0][0];
}
