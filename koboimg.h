#ifndef _KOBOIMG_H_
#define _KOBOIMG_H_

typedef struct
{
   int x0, y0, x1, y1;
} BoundingBox;

void init(int landscape);
int getWidth(void);
int getHeight(void);
void imgLine(int x, int y, int xx, int yy);
void imgBox(int x, int y, int xx, int yy);
BoundingBox drawString(char* str, int x, int y, int centered, int border);
unsigned char* create565(int landscape);

#endif
