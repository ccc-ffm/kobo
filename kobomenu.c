#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <include/linux/input.h>
#include "koboimg.h"

#define BORDER 24
#define BORDERIN 8

char stringBuffer[4096];

char menu[16][67]; // button -> label

int main(int argc, char* argv[])
{
   int landscape = 0;
   int flush = 0;
   int title = 0;

   for(int i = 1; i < argc; i++)
   {
      if (strcmp(argv[i], "-l") == 0)
      {
         landscape = 1;
      }
      else if (strcmp(argv[i], "-f") == 0)
      {
         flush = 1;
      }
      else if (strcmp(argv[i], "-t") == 0)
      {
         title = 1;
      }
      else
      {
         fprintf(stderr, "\n\
%s reads up to 16 semicolon-limited items to render as menu on the kobo, returns choice\n\
\n\
usage: %s [-l][-f][-t]\n\
\n\
options:\n\
   -l   landscape mode\n\
   -f   flush e-ink after touch\n\
   -t   first item is title\n\n", argv[0], argv[0]);
         exit(-1);
      }
   }
   
   init(landscape);
   
   int max = fread(stringBuffer, 1, 4096, stdin);
   fclose(stdin);

   int buttons = 0;
   int j = 0;
   for(int i = 0; i < max; i++)
   { 
      if(stringBuffer[i] =='\0')
      {
         menu[buttons][j] = '\0';
         break;
      }
      else if(stringBuffer[i] == ';')
      {
         menu[buttons][j] = '\0';
         buttons++;
         j = 0;
         if(buttons == 16) // wir können nur 16
         {
            break;
         }
      }
      else
      {
         if(j < 66)
         {
            menu[buttons][j] = stringBuffer[i];
            j++;
         }
      }
   }
   
   int buttonh = (getHeight() - 2 * BORDER) / buttons;
   int buttonl = BORDER + BORDERIN;
   int buttonr = getWidth() - BORDER - BORDERIN;
   int buttont = BORDER + BORDERIN;
   int buttonb = BORDER - BORDERIN + buttonh;
   int buttoncx = (buttonr + buttonl) / 2;
   int buttoncy = (buttont + buttonb) / 2;

   for(int i = 0; i < buttons; i++)
   {
     drawString(menu[i], buttoncx + 1, buttoncy + 1, 1, BORDER);
     if (i > 0 || !title)
     {
        imgBox(buttonl, buttont, buttonr, buttonb);
        imgBox(buttonl - 1, buttont - 1, buttonr + 1, buttonb + 1);
     }
     buttont += buttonh;
     buttonb += buttonh;
     buttoncy += buttonh;
   }

   buttont = BORDER + BORDERIN;
   buttonb = BORDER - BORDERIN + buttonh;
   
   FILE* pickel = popen("/usr/local/Kobo/pickel showpic", "w");
   fwrite(create565(landscape), 2, 800 * 600, pickel);
   pclose(pickel);
   
   //TASTENDRUCK LESEN

   FILE* fevent = fopen("/dev/input/event1", "rb");
   if(fevent == NULL){
      printf("Need /dev/input/event1 to read events\n");
      exit(-1);
   }

   struct input_event ie;

   int x_pos = 0;
   int y_pos = 0;
   int btn = -1;
   
   for(;;)
   {
      fread(&ie, 16, 1, fevent);
      if (ie.type == EV_SYN)
      {
         continue;
      }
      else if (ie.type == EV_KEY)
      {
         if (ie.code == BTN_TOUCH && ie.value == 0) // Finger event 
         {
            btn = (y_pos - BORDER) / buttonh;
            if (btn >= (title ? 1 : 0) && x_pos >= buttonl && x_pos <= buttonr && y_pos - btn * buttonh >= buttont && y_pos - btn * buttonh <= buttonb)
            {
               if(title) btn -=1;
               break;
            }
         }
      }
      else if (ie.type == EV_ABS)
      {
         if (ie.code == ABS_X) //X POS
         {
            if(landscape) x_pos = ie.value;
            else y_pos = ie.value;
         }
         else if (ie.code == ABS_Y) //Y POS
         {
            if(landscape) y_pos = ie.value;
            else x_pos = 599 - ie.value;
         }
      }
   }
   fclose(fevent);
   
   if (flush)
   {
      init(landscape);
      pickel = popen("/usr/local/Kobo/pickel showpic", "w");
      fwrite(create565(landscape), 2, 800 * 600, pickel);
      pclose(pickel);
   }

   printf("%d", btn);
   return btn;
}
