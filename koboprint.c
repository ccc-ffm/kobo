#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "koboimg.h"

#define BORDER 8
#define ADDTOP 6

char stringBuffer[4096];

int main(int argc, char* argv[])
{
   int landscape = 0;
   int output = 0;
   
   for(int i = 1; i < argc; i++)
   {
      if (strcmp(argv[i], "-l") == 0)
      {
         landscape = 1;
      }
      else if (strcmp(argv[i], "-o") == 0)
      {
         output = 1;
      }
      else
      {
         fprintf(stderr, "\n\
%s reads up to 4096 characters from stdin and renders the text on the kobo\n\
\n\
usage: %s [-l][-o]\n\
\n\
options:\n\
   -l   landscape mode\n\
   -o   output to stdout instead of e-ink\n\n", argv[0], argv[0]);
         exit(-1);
      }
   }
   
   init(landscape);
   
   fread(stringBuffer, 1, 4096, stdin);
   fclose(stdin);
   
   drawString(stringBuffer, BORDER, BORDER + ADDTOP, 0, BORDER);


   if(output)
   {
      fwrite(create565(landscape), 2, 800 * 600, stdout);
   }
   else
   {
      FILE* pickel = popen("/usr/local/Kobo/pickel showpic", "w");
      fwrite(create565(landscape), 2, 800 * 600, pickel);
      pclose(pickel);
   }
   
   return 0;
}
